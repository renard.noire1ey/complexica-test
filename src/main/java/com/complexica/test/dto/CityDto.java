package com.complexica.test.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CityDto {
    private float id;
    private String name;
    CoordDto coord;
    private String country;
    private float population;
    private float timezone;
    private float sunrise;
    private float sunset;
}
