package com.complexica.test.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WeatherResponseDto {
    private String cod;
    private float message;
    private float cnt;
    ArrayList<WeatherRecordDto> list = new ArrayList <> ();
    CityDto city;
}


