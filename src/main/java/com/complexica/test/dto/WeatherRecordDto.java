package com.complexica.test.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WeatherRecordDto {
    private float dt;
    Main main;
    ArrayList<Weather> weather = new ArrayList<> ();
    Clouds clouds;
    Wind wind;
    private float visibility;
    private float pop;
    Rain rain;
    Sys sys;
    private String dt_txt;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Sys {
        private String pod;

    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Weather {
        private float id;
        private String main;
        private String description;
        private String icon;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Rain {
        @JsonAlias("3h")
        private float threeHours;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Wind {
        private float speed;
        private float deg;
        private float gust;

    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Clouds {
        private float all;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Main {
        private float temp;
        private float feels_like;
        private float temp_min;
        private float temp_max;
        private float pressure;
        private float sea_level;
        private float grnd_level;
        private float humidity;
        private float temp_kf;
    }
}
