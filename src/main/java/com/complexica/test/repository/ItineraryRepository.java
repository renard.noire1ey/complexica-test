package com.complexica.test.repository;

import com.complexica.test.model.ItineraryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Repository
public interface ItineraryRepository extends JpaRepository<ItineraryEntity, Long> {

    @Modifying
    @Transactional
    void deleteAllByCachedOnBefore(LocalDateTime cachedOn);

}
