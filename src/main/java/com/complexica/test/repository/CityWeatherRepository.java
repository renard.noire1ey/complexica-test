package com.complexica.test.repository;

import com.complexica.test.model.CityWeatherEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.stereotype.Repository;


import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface CityWeatherRepository extends JpaRepository<CityWeatherEntity, Long> {
    List<CityWeatherEntity> findAllByCityName(String cityName);

    List<CityWeatherEntity> findAllByIdIn(List<Long> ids);

    List<CityWeatherEntity> findAllByCityNameAndDateAfter(String cityName, LocalDateTime date);

    @Modifying
    @Transactional
    void deleteAllByCachedOnBefore(LocalDateTime cachedOn);
}
