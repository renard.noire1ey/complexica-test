package com.complexica.test.utils;

import com.complexica.test.utils.annotations.Utility;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Objects;
import java.util.Set;

@Data
@Utility
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class WeatherForecastUrlBuilder {
    private final String API_URL;
    private final String API_KEY;

    private static final String API_KEY_PARAM = "appid";
    private static final String QUERY_PARAM = "q";
    private static final String UNITS_PARAM = "units";

    @Getter
    @NoArgsConstructor
    public enum Units {
        Standard("standard"),
        Metric("metric"),
        Imperial("imperial");

        Units(String value) {
            this.value = value;
        }

        private String value;
    }

    private final LinkedHashMap<String, Set<String>> urlParams;

    public WeatherForecastUrlBuilder(
            @Value("${application.weather-api.domain}") String apiUrl,
            @Value("${application.weather-api.api-key}") String apiKey
    ) {
        this.API_URL = apiUrl;
        this.API_KEY = apiKey;
        urlParams = new LinkedHashMap<>();
    }

    private void createRecordIfEmptyAndAdd(String paramName, String paramValue) {
        if (Objects.isNull(urlParams.get(paramName))) {
            urlParams.put(paramName, new HashSet<>());
        }
        urlParams.get(paramName).add(paramValue);
    }

    public WeatherForecastUrlBuilder query(String q) {
        createRecordIfEmptyAndAdd(QUERY_PARAM, q);
        return this;
    }

    public WeatherForecastUrlBuilder units(Units unit) {
        createRecordIfEmptyAndAdd(UNITS_PARAM, unit.getValue());
        return this;
    }

    public URI build() {
        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUriString(API_URL);
        urlParams.forEach((key, valueSet) ->
                uriComponentsBuilder.queryParam(key, String.join(",", valueSet))
        );
        uriComponentsBuilder.queryParam(API_KEY_PARAM, API_KEY);
        return uriComponentsBuilder.build().toUri();
    }
}