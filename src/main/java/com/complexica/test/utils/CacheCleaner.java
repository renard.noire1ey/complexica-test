package com.complexica.test.utils;

import com.complexica.test.repository.CityWeatherRepository;
import com.complexica.test.repository.ItineraryRepository;
import com.complexica.test.utils.annotations.Utility;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.LocalDateTime;

@AllArgsConstructor
@Utility
public class CacheCleaner {
    private final CityWeatherRepository cityWeatherRepository;
    private final ItineraryRepository itineraryRepository;

    @Scheduled(cron = "1 * * * * *")
    public void cleanCache() {
        LocalDateTime timeToDie = LocalDateTime.now().minusHours(1);
        itineraryRepository.deleteAllByCachedOnBefore(timeToDie);
        cityWeatherRepository.deleteAllByCachedOnBefore(timeToDie);
    }
}

