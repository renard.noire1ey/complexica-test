package com.complexica.test.utils;

import com.complexica.test.dto.WeatherRecordDto;
import com.complexica.test.dto.WeatherResponseDto;
import com.complexica.test.model.CityWeatherEntity;
import com.complexica.test.utils.annotations.Utility;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Utility
public class WeatherDtoToCityWeatherMapper {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public List<CityWeatherEntity> map(WeatherResponseDto weatherResponseDto) {
        String name = weatherResponseDto.getCity().getName();
        String country = weatherResponseDto.getCity().getCountry();


        return weatherResponseDto.getList().stream().map(listItem -> {
            Optional<WeatherRecordDto.Weather> weather = listItem.getWeather().stream().findFirst();

            CityWeatherEntity.CityWeatherEntityBuilder cityWeatherEntityBuilder = CityWeatherEntity.builder()
                    .cityName(name)
                    .countryCode(country)
                    .date(LocalDateTime.parse(listItem.getDt_txt(), formatter))
                    .temp(listItem.getMain().getTemp())
                    .feelsLike(listItem.getMain().getFeels_like());

            weather.ifPresent(weatherItem -> {
                cityWeatherEntityBuilder.weatherMain(weatherItem.getMain());
                cityWeatherEntityBuilder.weatherDescription(weatherItem.getDescription());
            });

            return cityWeatherEntityBuilder.build();
        }).collect(Collectors.toList());
    }
}
