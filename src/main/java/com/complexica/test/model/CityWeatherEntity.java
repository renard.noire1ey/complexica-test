package com.complexica.test.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="city_cache")
@EntityListeners(AuditingEntityListener.class)
public class CityWeatherEntity {
    private static final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @CreatedDate
    private LocalDateTime cachedOn;

    private String cityName;
    private String countryCode;

    private LocalDateTime date;
    private float temp;
    private float feelsLike;
    private String weatherMain;
    private String weatherDescription;

    public boolean itIsAcceptableTime() {
        // add all records between 12pm and 6pm to the table.
        return date.getHour() > 11 && date.getHour() < 17;
    }
}
