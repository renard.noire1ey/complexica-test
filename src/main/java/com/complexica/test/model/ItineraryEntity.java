package com.complexica.test.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="itinerary")
@EntityListeners(AuditingEntityListener.class)
public class ItineraryEntity {

    @CreatedDate
    private LocalDateTime cachedOn;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @JoinColumn(name="itinerary_id")
    @OneToMany
    private List<CityWeatherEntity> records;

    private String name;
}
