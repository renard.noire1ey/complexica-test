package com.complexica.test.controller.restcontroller;

import com.complexica.test.model.ItineraryEntity;
import com.complexica.test.repository.ItineraryRepository;
import com.complexica.test.service.ItineraryService;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Data
@CrossOrigin
@AllArgsConstructor
@RestController
@RequestMapping("/itinerary")
public class ItineraryController {
    private final ItineraryService itineraryService;

    @PostMapping
    public List<ItineraryEntity> createItinerary(@RequestParam String name, @RequestParam List<Long> ids) {
        return itineraryService.createItinerary(name, ids);
    }

    @GetMapping
    public List<ItineraryEntity> getAll(@RequestParam String name, @RequestParam List<Long> ids) {
        return itineraryService.createItinerary(name, ids);
    }
}
