package com.complexica.test.controller.restcontroller;

import com.complexica.test.model.CityWeatherEntity;
import com.complexica.test.service.WeatherService;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@Data
@CrossOrigin
@AllArgsConstructor
@RestController
@RequestMapping("/weather")
public class WeatherController {
    private final WeatherService weatherService;

    @GetMapping
    public List<CityWeatherEntity> getWeather(
            @RequestParam String city,
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime time
    ) {
        return weatherService.getAllForCityAndTimeAfter(city, time);
    }
}
