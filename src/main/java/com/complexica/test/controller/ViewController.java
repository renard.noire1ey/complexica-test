package com.complexica.test.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

@Data
@AllArgsConstructor
@Controller
public class ViewController {
    private RestTemplate restTemplate;

    @RequestMapping("/")
    public ModelAndView homePage() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("index.html");
        return mav;
    }
}
