package com.complexica.test.service;

import com.complexica.test.model.ItineraryEntity;

import java.util.List;

public interface ItineraryService {
    List<ItineraryEntity> createItinerary(String name, List<Long> ids);

    List<ItineraryEntity> getAll();

    void removeItinerary(long id);
}
