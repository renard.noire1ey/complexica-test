package com.complexica.test.service;

import com.complexica.test.dto.WeatherResponseDto;

public interface WeatherReceiverService {
    WeatherResponseDto getWeatherForCity(String cityName);
}
