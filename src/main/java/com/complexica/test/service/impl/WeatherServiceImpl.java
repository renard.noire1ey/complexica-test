package com.complexica.test.service.impl;

import com.complexica.test.dto.WeatherResponseDto;
import com.complexica.test.model.CityWeatherEntity;
import com.complexica.test.repository.CityWeatherRepository;
import com.complexica.test.service.WeatherReceiverService;
import com.complexica.test.service.WeatherService;
import com.complexica.test.utils.WeatherDtoToCityWeatherMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Service
@AllArgsConstructor
public class WeatherServiceImpl implements WeatherService {
    private final CityWeatherRepository cityWeatherRepository;
    private final WeatherReceiverService weatherReceiverService;
    private final WeatherDtoToCityWeatherMapper weatherMapper;


    @Override
    public List<CityWeatherEntity> getAll() {
        return cityWeatherRepository.findAll();
    }

    @Override
    public List<CityWeatherEntity> getAllForCity(String cityName) {
        List<CityWeatherEntity> entities = cityWeatherRepository.findAllByCityName(cityName);
        if (!entities.isEmpty()) {
            return entities;
        }

        List<CityWeatherEntity> list = weatherMapper.map(weatherReceiverService.getWeatherForCity(cityName))
                .stream().filter(CityWeatherEntity::itIsAcceptableTime).collect(Collectors.toList()
        );

        return cityWeatherRepository.saveAll(list);
    }

    @Override
    public List<CityWeatherEntity> getAllForCityAndTimeAfter(String cityName, LocalDateTime time) {
        List<CityWeatherEntity> entities = cityWeatherRepository.findAllByCityNameAndDateAfter(cityName, time);
        if (!entities.isEmpty()) {
            return entities;
        }

        WeatherResponseDto weatherForCity = weatherReceiverService.getWeatherForCity(cityName);
        List<CityWeatherEntity> list = weatherMapper.map(weatherForCity)
                .stream().filter(CityWeatherEntity::itIsAcceptableTime).collect(Collectors.toList());
        cityWeatherRepository.saveAll(list);

        return cityWeatherRepository.findAllByCityNameAndDateAfter(cityName, time);
    }

}
