package com.complexica.test.service.impl;

import com.complexica.test.dto.WeatherResponseDto;
import com.complexica.test.service.WeatherReceiverService;
import com.complexica.test.utils.WeatherForecastUrlBuilder;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Service
@AllArgsConstructor
public class WeatherReceiverServiceImpl implements WeatherReceiverService {
    private final RestTemplate restTemplate;
    private final WeatherForecastUrlBuilder weatherForecastUrlBuilder;

    @Override
    public WeatherResponseDto getWeatherForCity(String cityName) {
        URI uri = weatherForecastUrlBuilder
                .query(cityName)
                .units(WeatherForecastUrlBuilder.Units.Metric)
                .build();
        return restTemplate.getForEntity(
                uri, WeatherResponseDto.class
        ).getBody();
    }
}
