package com.complexica.test.service.impl;

import com.complexica.test.model.CityWeatherEntity;
import com.complexica.test.model.ItineraryEntity;
import com.complexica.test.repository.CityWeatherRepository;
import com.complexica.test.repository.ItineraryRepository;
import com.complexica.test.service.ItineraryService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class ItineraryServiceImpl implements ItineraryService {
    private final ItineraryRepository itineraryRepository;
    private final CityWeatherRepository cityWeatherRepository;

    @Override
    public List<ItineraryEntity> createItinerary(String name, List<Long> ids) {
        List<CityWeatherEntity> cityRecords = cityWeatherRepository.findAllByIdIn(ids);
        ItineraryEntity entity = new ItineraryEntity();
        entity.setName(name);
        entity.setRecords(cityRecords);
        itineraryRepository.save(entity);
        return itineraryRepository.findAll();
    }

    @Override
    public void removeItinerary(long id) {
        itineraryRepository.deleteById(id);
    }

    @Override
    public List<ItineraryEntity> getAll() {
        return itineraryRepository.findAll();
    }
}
