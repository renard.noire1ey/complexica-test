package com.complexica.test.service;

import com.complexica.test.model.CityWeatherEntity;

import java.time.LocalDateTime;
import java.util.List;

public interface WeatherService {
    List<CityWeatherEntity> getAll();

    List<CityWeatherEntity> getAllForCity(String cityName);

    List<CityWeatherEntity> getAllForCityAndTimeAfter(String cityName, LocalDateTime time);
}
