import { AxiosInstance } from "axios";
import { WeatherRecord } from "../types";


export class WeatherApi {

  constructor(
    private readonly axios: AxiosInstance,
    private readonly baseUrl = "/weather",
  ) {}

  public async getWeather({ city, time }: { city: string, time: string }): Promise<Array<WeatherRecord>> {
    const params = new URLSearchParams();
    city && params.set('city', city);
    time && params.set('time', time);

    const { data } = await this.axios.get<Array<WeatherRecord>>(`${this.baseUrl}?${params}`)
    return data
  };
}
