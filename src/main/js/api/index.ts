// @ts-ignore
import axios from 'axios';

export const axiosInstance = axios.create({
  params: {} // do not remove this, its added to add params later in the config
});
axiosInstance.defaults.baseURL = process.env.NODE_ENV === 'production' ? process.env.PUBLIC_URL : 'http://localhost:8080';

export * from './weather';
export * from './itinerary';

