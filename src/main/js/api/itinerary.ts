import { Itinerary } from "../types";
import { AxiosInstance } from "axios";

export class ItineraryApi {
  constructor(
    private readonly axios: AxiosInstance,
    private readonly baseUrl = "/itinerary",
  ) {}

  public async createItinerary({ ids }: { ids: Array<string> }): Promise<Array<Itinerary>> {
    const params = new URLSearchParams();
    params.set('ids', ids.join())

    const { data } = await this.axios.post<Array<Itinerary>>(`${this.baseUrl}?${params}`)
    return data
  };

}