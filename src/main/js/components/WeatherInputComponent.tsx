import React, { ChangeEvent, FormEvent, useState } from 'react';
import { connect } from "react-redux";
import styled from "@emotion/styled";
import moment from "moment";
import { Button, TextField } from "@material-ui/core";
import { getWeatherInfo } from "../store/actions";

const WeatherInputComponentWrapper = styled.form`
  display: flex;
  position: relative;
  padding: 40px;
  align-items: flex-end;
  justify-content: flex-end;
  /* border: 1px solid gray; */
  border-radius: 15px;
  margin-bottom: 40px;
  box-shadow: 0 0 30px grey;

  & > * {
    &:not(:first-child) {
      margin-left: 10px;
    }
  }

  #date {
    padding: 5px;
    width: 13.5em;
  }

  #city-list {
    width: 20em;
  }
`
const formatDate = (date: Date) => {
  return moment(date).format("yyyy-MM-DD")
}

const WeatherInputComponent = ({ getWeatherInfo }: any) => {
  const [ state, setState ] = useState(() => ({
    city: '',
    date: formatDate(new Date())
  }))

  const stateSetter = (name: string, value: unknown) => {
    if (name) {
      setState(prevState => ({
        ...prevState,
        [name]: value
      }))
    }
  }

  const onChange = ({ target: { name, value } }: ChangeEvent<HTMLInputElement>) => {
    stateSetter(name, value)
  }

  const onSubmit = (e: FormEvent) => {
    e.preventDefault();
    getWeatherInfo(state.city, moment.utc(state.date).toISOString());
  }

  const hasCityError = !state.city && !state.city.trim()
  const hasDateError = !state.date;
  const minDate = formatDate(new Date());
  const maxDate = formatDate(moment().add(4, 'day').endOf('day').toDate())

  return (
    <WeatherInputComponentWrapper onSubmit={ onSubmit }>
      <TextField
        id="city-name"
        name="city"
        onChange={ onChange }
        error={hasCityError}
        label={hasCityError ? 'Enter city name' : ''}

      />

      <input
        id="date"
        type="date"
        name="date"
        onChange={ onChange }
        min={minDate}
        max={maxDate}
        value={state.date}
      />
      <div className="button-section">
        <Button
          type="submit"
          color="primary"
          variant="contained"
          disabled={hasCityError || hasDateError}
        >
          Submit
        </Button>
      </div>
    </WeatherInputComponentWrapper>
  );
};

export default connect(
  null,
  (dispatch: any) => {
    return {
      getWeatherInfo: async (cityName: string, time: string) => {
        dispatch(getWeatherInfo(cityName, time))
      }
    }
  }
)(WeatherInputComponent);
