import React from 'react';
import { useSelector } from 'react-redux';
import { DataGrid } from '@material-ui/data-grid';
import styled from '@emotion/styled';
import moment from "moment";

const WeatherTableComponentWrapper = styled.div`
  position: relative;
  width: 80vw;
  height: 80vh;
`
const columns = [
  {
    field: "cityName",
    headerName: 'City Name',
    width: 250
  },
  {
    field: "countryCode",
    headerName: 'Country Code',
    width: 250
  },
  {
    field: "temp",
    headerName: 'Temperature (°C)',
    width: 250
  },
  {
    field: "feelsLike",
    headerName: 'Feels like (°C)',
    width: 250
  },
  {
    field: "date",
    headerName: 'Time',
    width: 250,
    valueGetter: (params: any) => {
      return moment.utc(params.row.date).local().format("MM/DD, dddd yyyy HH:mm:ss")
    }
  },
  {
    field: "weather",
    headerName: 'Weather',
    width: 400,
    valueGetter: (params: any) => {
      const { weatherMain, weatherDescription } = params.row;
      return `${ weatherMain }, ${ weatherDescription }`
    }
  },
]

export const WeatherTableComponent = () => {
  const weatherRecords = useSelector((state: any) => state.weather.weatherRecords)

  return (
    <WeatherTableComponentWrapper>
      <DataGrid rows={ weatherRecords } columns={ columns }/>
    </WeatherTableComponentWrapper>
  );
};
