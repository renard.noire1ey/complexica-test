export interface WeatherRecord {
  id: number;
  cachedOn: string;
  cityName: string;
  countryCode: string;
  date: string;
  temp: number;
  feelsLike: number;
  weatherMain: string;
  weatherDescription: string
}

export interface Itinerary {
  cachedOn: string;
  id: number;
  records: Array<WeatherRecord>;
  name: String;
}