import React from 'react';
import { Provider } from 'react-redux';
import WeatherPage from "./pages/WeatherPage";
import { store } from "./store/configureStore";
import { ToastContainer } from "react-toastify";

import 'react-toastify/dist/ReactToastify.css';
import styled from "@emotion/styled";

const AppWrapper = styled.div`
  h1 {
    text-align: center;
  }
`

function App() {
  return (
    <Provider store={store}>
      <AppWrapper className="App">
        <header className="App-header">
          <h1>Weather App</h1>
        </header>
        <WeatherPage />
      </AppWrapper>
      <ToastContainer />
    </Provider>

  );
}

export default App;
