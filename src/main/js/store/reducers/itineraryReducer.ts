import { WeatherActions } from "../types";

const initialState = {
  weatherRecords: []
}

export default function WeatherReducer(state: any = initialState, action: any) {
  switch (action.type as WeatherActions) {
    case WeatherActions.GET_WEATHER_SUCCESS:
      return {
        ...state,
        weatherRecords: [ ...action.weatherRecords, ...state.weatherRecords ]
      }
    default:
      return state;
  }
}