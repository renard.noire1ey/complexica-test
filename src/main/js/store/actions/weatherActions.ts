import { WeatherActions } from '../types';
import { WeatherApi } from '../../api';
import { toast } from "react-toastify";

function getWeatherStarted() {
  return {
    type: WeatherActions.GET_WEATHER_STARTED,
  }
}

function getWeatherSuccess(weatherRecords: Array<any>) {
  return {
    type: WeatherActions.GET_WEATHER_SUCCESS,
    weatherRecords
  }
}

function getWeatherFailure(errorMsg: string) {
  return {
    type: WeatherActions.GET_WEATHER_FAILURE,
    errorMsg
  }
}

export function getWeatherInfo(city: string, time: string) {
  return function (dispatch: any, getState: () => any, { weatherApi }: { weatherApi: WeatherApi }) {
    dispatch(getWeatherStarted())
    weatherApi.getWeather({ city, time }).then(
      (data) => {
        if (data.length === 0) {
          toast.info('No results')
        } else {
          toast.success('Updated!')
        }
        dispatch(getWeatherSuccess(data))
      },
      (err) => {
        toast.error(err.message)
        dispatch(getWeatherFailure(err.message))
      }
    )
  }
}
