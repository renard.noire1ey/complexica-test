import { ItineraryApi } from "../../api";
import { Itinerary } from "../../types";
import { toast } from "react-toastify";
import { ItineraryActions } from "../types";
import { batch } from "react-redux";


function setItineraries(itineraries: Array<Itinerary>) {
  return {
    type: ItineraryActions.SET_ITINERARIES,
    itineraries
  }
}

function createItinerarySuccess() {
  return {
    type: ItineraryActions.CREATE_ITINERARY,
  }
}


export function createItinerary(ids: Array<string>) {
  return function (dispatch: any, getState: () => any, { itineraryApi }: { itineraryApi: ItineraryApi }) {
    itineraryApi.createItinerary({
      ids
    }).then(
      (itineraries: Array<Itinerary>) => {
        batch(() => {
          dispatch(createItinerarySuccess())
          dispatch(setItineraries(itineraries));
        })
        toast.success('Itinerary Created!')
      },
      (err) => {
        toast.error(err.message)
      }
    )
  }
}