import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import rootReducer from './reducers/index';
import { axiosInstance, ItineraryApi, WeatherApi } from '../api';

export const store = createStore(rootReducer,
 composeWithDevTools(
   applyMiddleware(
     thunk
       .withExtraArgument({
         weatherApi: new WeatherApi(axiosInstance),
         itineraryApi: new ItineraryApi(axiosInstance)
       })
   ),
 )
);