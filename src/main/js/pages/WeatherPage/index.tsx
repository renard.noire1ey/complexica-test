import React from 'react';
import styled from "@emotion/styled";
import WeatherInputComponent from "../../components/WeatherInputComponent";
import { WeatherTableComponent } from "../../components/WeatherTableComponent";
import { Button } from "@material-ui/core";
import { connect } from "react-redux";
import { createItinerary } from "../../store/actions";
import { WeatherRecord } from "../../types";

const WeatherPageWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`

const ActionSectionWrapper = styled.section`
  display: flex;
  flex-direction: row;
  align-items: flex-end;
`

interface WeatherPageProps {
  createItinerary?: (ids: Array<string>) => void;
  weatherRecords?: Array<WeatherRecord>;
}

const WeatherPage = ({ createItinerary, weatherRecords = [] }: WeatherPageProps) => {

  const onCreateItineraryClick = () => {
    createItinerary && createItinerary(weatherRecords.map(item => item.id + ''))
  }

  return (
    <WeatherPageWrapper>
      <ActionSectionWrapper>
        <WeatherInputComponent/>
        {/*<Button onClick={onCreateItineraryClick}>Create itinerary</Button>*/ }
      </ActionSectionWrapper>
      <WeatherTableComponent/>
    </WeatherPageWrapper>
  );
};

export default connect(
  (state: any) => ({
    weatherRecords: state.weather.weatherRecords
  }),
  (dispatch: any) => {
    return {
      createItinerary: (weatherIds: Array<string>) => (
        dispatch(createItinerary(weatherIds))
      )
    }
  }
)(WeatherPage)
