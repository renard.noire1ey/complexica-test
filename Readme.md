# Weather APP

### Isomorphic application created with JavaSpring & TypeReactRedux.
### Dependencies
    - Maven 3.11
    - Java 8
    - node, yarn, react - they all are managed only by Maven - so in the best case, you shouldn't have problems with build
### To run this application type in console:
```mvn clean install```

### Then you can run from IDE or run from tomcat or terminal


#### P.S This configuration took too much time, I did not have time to complete the task completely
